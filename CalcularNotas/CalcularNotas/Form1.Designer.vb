﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelTitle = New System.Windows.Forms.Label()
        Me.LabelEv1 = New System.Windows.Forms.Label()
        Me.LabelEv2 = New System.Windows.Forms.Label()
        Me.LabelEv3 = New System.Windows.Forms.Label()
        Me.LabelPresentacion = New System.Windows.Forms.Label()
        Me.LabelAsistencia = New System.Windows.Forms.Label()
        Me.LabelExamen = New System.Windows.Forms.Label()
        Me.LabelFinal = New System.Windows.Forms.Label()
        Me.ButtonNExamen = New System.Windows.Forms.Button()
        Me.ButtonAprobar = New System.Windows.Forms.Button()
        Me.ButtonFinal = New System.Windows.Forms.Button()
        Me.ComboBoxEv1 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxEv2 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxEv3 = New System.Windows.Forms.ComboBox()
        Me.TextBoxPresentacion = New System.Windows.Forms.TextBox()
        Me.TextBoxExamen = New System.Windows.Forms.TextBox()
        Me.TextBoxFinal = New System.Windows.Forms.TextBox()
        Me.NumericUpDownAsistencia = New System.Windows.Forms.NumericUpDown()
        CType(Me.NumericUpDownAsistencia, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelTitle
        '
        Me.LabelTitle.AutoSize = True
        Me.LabelTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTitle.Location = New System.Drawing.Point(75, 9)
        Me.LabelTitle.Name = "LabelTitle"
        Me.LabelTitle.Size = New System.Drawing.Size(226, 25)
        Me.LabelTitle.TabIndex = 0
        Me.LabelTitle.Text = "Calculador de Notas"
        '
        'LabelEv1
        '
        Me.LabelEv1.AutoSize = True
        Me.LabelEv1.Location = New System.Drawing.Point(19, 58)
        Me.LabelEv1.Name = "LabelEv1"
        Me.LabelEv1.Size = New System.Drawing.Size(89, 13)
        Me.LabelEv1.TabIndex = 1
        Me.LabelEv1.Text = "Evaluación1 25%"
        '
        'LabelEv2
        '
        Me.LabelEv2.AutoSize = True
        Me.LabelEv2.Location = New System.Drawing.Point(19, 87)
        Me.LabelEv2.Name = "LabelEv2"
        Me.LabelEv2.Size = New System.Drawing.Size(89, 13)
        Me.LabelEv2.TabIndex = 2
        Me.LabelEv2.Text = "Evaluación2 35%"
        '
        'LabelEv3
        '
        Me.LabelEv3.AutoSize = True
        Me.LabelEv3.Location = New System.Drawing.Point(19, 115)
        Me.LabelEv3.Name = "LabelEv3"
        Me.LabelEv3.Size = New System.Drawing.Size(89, 13)
        Me.LabelEv3.TabIndex = 3
        Me.LabelEv3.Text = "Evaluación3 40%"
        '
        'LabelPresentacion
        '
        Me.LabelPresentacion.AutoSize = True
        Me.LabelPresentacion.Location = New System.Drawing.Point(19, 171)
        Me.LabelPresentacion.Name = "LabelPresentacion"
        Me.LabelPresentacion.Size = New System.Drawing.Size(110, 13)
        Me.LabelPresentacion.TabIndex = 4
        Me.LabelPresentacion.Text = "Nota de Presentacion"
        '
        'LabelAsistencia
        '
        Me.LabelAsistencia.AutoSize = True
        Me.LabelAsistencia.Location = New System.Drawing.Point(19, 197)
        Me.LabelAsistencia.Name = "LabelAsistencia"
        Me.LabelAsistencia.Size = New System.Drawing.Size(55, 13)
        Me.LabelAsistencia.TabIndex = 5
        Me.LabelAsistencia.Text = "Asistencia"
        '
        'LabelExamen
        '
        Me.LabelExamen.AutoSize = True
        Me.LabelExamen.Location = New System.Drawing.Point(25, 256)
        Me.LabelExamen.Name = "LabelExamen"
        Me.LabelExamen.Size = New System.Drawing.Size(86, 13)
        Me.LabelExamen.TabIndex = 6
        Me.LabelExamen.Text = "Nota de Examen"
        '
        'LabelFinal
        '
        Me.LabelFinal.AutoSize = True
        Me.LabelFinal.Location = New System.Drawing.Point(19, 316)
        Me.LabelFinal.Name = "LabelFinal"
        Me.LabelFinal.Size = New System.Drawing.Size(55, 13)
        Me.LabelFinal.TabIndex = 7
        Me.LabelFinal.Text = "Nota Final"
        '
        'ButtonNExamen
        '
        Me.ButtonNExamen.Location = New System.Drawing.Point(19, 138)
        Me.ButtonNExamen.Name = "ButtonNExamen"
        Me.ButtonNExamen.Size = New System.Drawing.Size(321, 23)
        Me.ButtonNExamen.TabIndex = 8
        Me.ButtonNExamen.Text = "Calcular Nota Examen"
        Me.ButtonNExamen.UseVisualStyleBackColor = True
        '
        'ButtonAprobar
        '
        Me.ButtonAprobar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAprobar.Location = New System.Drawing.Point(22, 216)
        Me.ButtonAprobar.Name = "ButtonAprobar"
        Me.ButtonAprobar.Size = New System.Drawing.Size(321, 23)
        Me.ButtonAprobar.TabIndex = 9
        Me.ButtonAprobar.Text = "¿Cuanto necesito para aprobar?"
        Me.ButtonAprobar.UseVisualStyleBackColor = True
        '
        'ButtonFinal
        '
        Me.ButtonFinal.Location = New System.Drawing.Point(19, 280)
        Me.ButtonFinal.Name = "ButtonFinal"
        Me.ButtonFinal.Size = New System.Drawing.Size(321, 23)
        Me.ButtonFinal.TabIndex = 10
        Me.ButtonFinal.Text = "Calcular Nota Final"
        Me.ButtonFinal.UseVisualStyleBackColor = True
        '
        'ComboBoxEv1
        '
        Me.ComboBoxEv1.FormattingEnabled = True
        Me.ComboBoxEv1.Location = New System.Drawing.Point(200, 50)
        Me.ComboBoxEv1.Name = "ComboBoxEv1"
        Me.ComboBoxEv1.Size = New System.Drawing.Size(140, 21)
        Me.ComboBoxEv1.TabIndex = 11
        '
        'ComboBoxEv2
        '
        Me.ComboBoxEv2.FormattingEnabled = True
        Me.ComboBoxEv2.Location = New System.Drawing.Point(200, 79)
        Me.ComboBoxEv2.Name = "ComboBoxEv2"
        Me.ComboBoxEv2.Size = New System.Drawing.Size(140, 21)
        Me.ComboBoxEv2.TabIndex = 12
        '
        'ComboBoxEv3
        '
        Me.ComboBoxEv3.FormattingEnabled = True
        Me.ComboBoxEv3.Location = New System.Drawing.Point(200, 107)
        Me.ComboBoxEv3.Name = "ComboBoxEv3"
        Me.ComboBoxEv3.Size = New System.Drawing.Size(140, 21)
        Me.ComboBoxEv3.TabIndex = 13
        '
        'TextBoxPresentacion
        '
        Me.TextBoxPresentacion.Location = New System.Drawing.Point(200, 164)
        Me.TextBoxPresentacion.Name = "TextBoxPresentacion"
        Me.TextBoxPresentacion.Size = New System.Drawing.Size(140, 20)
        Me.TextBoxPresentacion.TabIndex = 14
        '
        'TextBoxExamen
        '
        Me.TextBoxExamen.Location = New System.Drawing.Point(200, 249)
        Me.TextBoxExamen.Name = "TextBoxExamen"
        Me.TextBoxExamen.Size = New System.Drawing.Size(140, 20)
        Me.TextBoxExamen.TabIndex = 15
        '
        'TextBoxFinal
        '
        Me.TextBoxFinal.Location = New System.Drawing.Point(200, 309)
        Me.TextBoxFinal.Name = "TextBoxFinal"
        Me.TextBoxFinal.Size = New System.Drawing.Size(140, 20)
        Me.TextBoxFinal.TabIndex = 16
        '
        'NumericUpDownAsistencia
        '
        Me.NumericUpDownAsistencia.Location = New System.Drawing.Point(200, 190)
        Me.NumericUpDownAsistencia.Name = "NumericUpDownAsistencia"
        Me.NumericUpDownAsistencia.Size = New System.Drawing.Size(140, 20)
        Me.NumericUpDownAsistencia.TabIndex = 17
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(362, 361)
        Me.Controls.Add(Me.NumericUpDownAsistencia)
        Me.Controls.Add(Me.TextBoxFinal)
        Me.Controls.Add(Me.TextBoxExamen)
        Me.Controls.Add(Me.TextBoxPresentacion)
        Me.Controls.Add(Me.ComboBoxEv3)
        Me.Controls.Add(Me.ComboBoxEv2)
        Me.Controls.Add(Me.ComboBoxEv1)
        Me.Controls.Add(Me.ButtonFinal)
        Me.Controls.Add(Me.ButtonAprobar)
        Me.Controls.Add(Me.ButtonNExamen)
        Me.Controls.Add(Me.LabelFinal)
        Me.Controls.Add(Me.LabelExamen)
        Me.Controls.Add(Me.LabelAsistencia)
        Me.Controls.Add(Me.LabelPresentacion)
        Me.Controls.Add(Me.LabelEv3)
        Me.Controls.Add(Me.LabelEv2)
        Me.Controls.Add(Me.LabelEv1)
        Me.Controls.Add(Me.LabelTitle)
        Me.Name = "Form1"
        Me.Text = "  "
        CType(Me.NumericUpDownAsistencia, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LabelTitle As Label
    Friend WithEvents LabelEv1 As Label
    Friend WithEvents LabelEv2 As Label
    Friend WithEvents LabelEv3 As Label
    Friend WithEvents LabelPresentacion As Label
    Friend WithEvents LabelAsistencia As Label
    Friend WithEvents LabelExamen As Label
    Friend WithEvents LabelFinal As Label
    Friend WithEvents ButtonNExamen As Button
    Friend WithEvents ButtonAprobar As Button
    Friend WithEvents ButtonFinal As Button
    Friend WithEvents ComboBoxEv1 As ComboBox
    Friend WithEvents ComboBoxEv2 As ComboBox
    Friend WithEvents ComboBoxEv3 As ComboBox
    Friend WithEvents TextBoxPresentacion As TextBox
    Friend WithEvents TextBoxExamen As TextBox
    Friend WithEvents TextBoxFinal As TextBox
    Friend WithEvents NumericUpDownAsistencia As NumericUpDown
End Class
