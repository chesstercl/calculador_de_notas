﻿Public Class Form1

    'Las notas de los combobox van desde 1.0 a 7.0

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim nota As Double
        For i = 1 To 6
            For j = 0 To 9
                nota = i + j / 10

                ComboBoxEv1.Items.Add(nota)
                ComboBoxEv2.Items.Add(nota)
                ComboBoxEv3.Items.Add(nota)
            Next
        Next
        ComboBoxEv1.Items.Add(7.0)
        ComboBoxEv2.Items.Add(7.0)
        ComboBoxEv3.Items.Add(7.0)
    End Sub


    'Al presionar el botón para calcular nota Examen, deberá validar que no falte ninguna nota en los combobox.

    Private Sub ButtonNExamen_Click(sender As Object, e As EventArgs) Handles ButtonNExamen.Click
        If (ComboBoxEv1.SelectedIndex = -1 OrElse ComboBoxEv2.SelectedIndex = -1 OrElse ComboBoxEv3.SelectedIndex = -1) Then
            MessageBox.Show("Debe tener notas en las 3 evalauciones")
        Else
            Dim notaEv1, notaEv2, notaEv3, notaPresentacion As Double

            notaEv1 = ComboBoxEv1.SelectedItem
            notaEv2 = ComboBoxEv2.SelectedItem
            notaEv3 = ComboBoxEv3.SelectedItem

            notaPresentacion = (notaEv1 * 0.25) + (notaEv2 * 0.35) + (notaEv3 * 0.4)
            notaPresentacion = Math.Round(notaPresentacion, 1)
            TextBoxPresentacion.Text = notaPresentacion


        End If
    End Sub


    'Al presionar el botón Cuanto necesito para aprobar, deberá calcular la nota mínima que necesita
    'en el examen para aprobar con un 4.0

    Private Sub ButtonAprobar_Click(sender As Object, e As EventArgs) Handles ButtonAprobar.Click
        Dim nota, notaPresentacion, notaFinal, notaNecesaria As Double

        If TextBoxPresentacion.Text = "" Then
            MessageBox.Show("Debe tener nota de presentacion para realizar el calculo")
        Else
            notaPresentacion = TextBoxPresentacion.Text
        End If

        Select Case notaPresentacion
            Case 1 To 1.9
                MessageBox.Show("No puede aprobar, su nota nota final es igual a su nota de presentacion: " & notaPresentacion)
                Exit Select

            Case 6.1 To 7
                MessageBox.Show("Si realiza el examen su nota final en cualquier caso sera mayor a 4")
                Exit Select

            Case 2 To 6
                For i = 1 To 6
                    For j = 0 To 9
                        nota = i + j / 10
                        notaFinal = (notaPresentacion * 0.6 + nota * 0.4)
                        notaFinal = Math.Round(notaFinal, 1)
                        If (notaFinal = 4) Then
                            notaNecesaria = nota
                            MessageBox.Show("Necesita un " & nota & " para aprobar con un 4,0")
                            Exit For

                        End If
                    Next

                    If (notaNecesaria > 0) Then Exit For
                Next
            Case Else
                MessageBox.Show("La nota de presentacion debe estar entre 1 y 7 para realizar el calculo")
        End Select


    End Sub

    'Al presionar el botón Calcular nota Final deberá obtener la nota final entre la nota de presentación
    '(60%) y el examen (40%)

    Private Sub ButtonFinal_Click(sender As Object, e As EventArgs) Handles ButtonFinal.Click
        Dim notaPresentacion, notaExamen, notaFinal As Double
        If (TextBoxExamen.Text = "" OrElse TextBoxPresentacion.Text = "") Then
            MessageBox.Show("Debe ingresar datos para realizar el calculo")
        Else
            notaPresentacion = TextBoxPresentacion.Text
            If notaPresentacion < 2 Then
                notaFinal = notaPresentacion
            End If
            notaExamen = TextBoxExamen.Text

            notaFinal = notaPresentacion * 0.6 + notaExamen * 0.4

            TextBoxFinal.Text = notaFinal

        End If
    End Sub

    Private Sub NumericUpDownAsistencia_ValueChanged(sender As Object, e As EventArgs) Handles NumericUpDownAsistencia.ValueChanged

        Dim ev1, ev2, ev3, notaPresentacion, asistencia As Double


        If TextBoxPresentacion.Text = "" Then
            MessageBox.Show("Debe tener nota de presentacion para realizar los calculos asociados a asistencia")
        Else
            notaPresentacion = TextBoxPresentacion.Text
        End If

        asistencia = NumericUpDownAsistencia.Value
        ev1 = ComboBoxEv1.SelectedItem
        ev2 = ComboBoxEv2.SelectedItem
        ev3 = ComboBoxEv3.SelectedItem

        If notaPresentacion >= 5.5 AndAlso ev1 >= 4 AndAlso ev2 >= 4 AndAlso ev3 >= 4 Then
            TextBoxExamen.Text = notaPresentacion

        ElseIf notaPresentacion >= 4 AndAlso asistencia >= 70 AndAlso ev1 >= 4 AndAlso ev2 >= 4 AndAlso ev3 >= 4 Then
            TextBoxExamen.Text = notaPresentacion
        Else
            TextBoxExamen.Text = ""
        End If
    End Sub

    Private Sub ComboBoxEv1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxEv1.SelectedIndexChanged

        Dim notaPresentacion
        notaPresentacion = TextBoxPresentacion.Text

        If Len(notaPresentacion) > 0 Then
            MessageBox.Show("Vuelva a calcular su nota de presentacion")
            TextBoxPresentacion.Text = ""
        End If

    End Sub

    Private Sub ComboBoxEv2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxEv2.SelectedIndexChanged

        Dim notaPresentacion
        notaPresentacion = TextBoxPresentacion.Text

        If Len(notaPresentacion) > 0 Then
            MessageBox.Show("Vuelva a calcular su nota de presentacion")
            TextBoxPresentacion.Text = ""
        End If

    End Sub

    Private Sub ComboBoxEv3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxEv3.SelectedIndexChanged

        Dim notaPresentacion
        notaPresentacion = TextBoxPresentacion.Text

        If Len(notaPresentacion) > 0 Then
            MessageBox.Show("Vuelva a calcular su nota de presentacion")
            TextBoxPresentacion.Text = ""
        End If

    End Sub
End Class

